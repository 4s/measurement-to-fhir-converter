import { expect } from 'chai'
import GPSLocation from 'src/gps-location'
import { MeasurementEvent } from 'src/measurement-event'

describe('GPS Location', () => {

    it('Create gpsLocation', () => {
        const gpsLocation = new GPSLocation(0, 27, 48, 45, 2);
        expect(gpsLocation.getLatitude()).to.equal(27);
        expect(gpsLocation.getLongitude()).to.equal(48);
        expect(gpsLocation.getAltitude()).to.equal(45);
        expect(gpsLocation.getHorizontalAccuracy()).to.equal(2);
        expect(gpsLocation.getTime()).to.equal(0);
        expect(gpsLocation.getStartTime()).to.equal(undefined);
        expect(gpsLocation.getEndTime()).to.equal(undefined);
    })

    it('Create gpsLocation without optional values', () => {
        const gpsLocation = new GPSLocation(0, 27, 48);
        expect(gpsLocation.getLatitude()).to.equal(27);
        expect(gpsLocation.getLongitude()).to.equal(48);
    })

    it('GPSLocation to MeasurementEvent', () => {
        const gpsLocation = new GPSLocation(0, 27, 48, 45, 2);
        const measurementEvent: MeasurementEvent = gpsLocation;
        expect(gpsLocation.getTime()).to.equal(measurementEvent.getTime());
        expect((measurementEvent as GPSLocation).getLatitude()).to.equal(gpsLocation.getLatitude());
        expect((measurementEvent as GPSLocation).getLongitude()).to.equal(gpsLocation.getLongitude());
        expect((measurementEvent as GPSLocation).getAltitude()).to.equal(gpsLocation.getAltitude());
        expect((measurementEvent as GPSLocation).getHorizontalAccuracy()).to.equal(gpsLocation.getHorizontalAccuracy());
    })
})