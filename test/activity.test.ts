import {expect} from 'chai'
import { Activity, ActivityType, ActivityConfidence} from 'src/activity'
import { MeasurementEvent } from 'src/measurement-event'

describe('Activity', () => {

    it('Create Activity', () => {
        const activity = new Activity(0,1,ActivityType.WALKING,ActivityConfidence.HIGH);
        expect(activity.getActivity()).to.equal(ActivityType.WALKING);
        expect(activity.getConfidence()).to.equal(ActivityConfidence.HIGH);
        expect(activity.getTime()).to.equal(undefined);
        expect(activity.getStartTime()).to.equal(0);
        expect(activity.getEndTime()).to.equal(1);
    })
    it('Create Activity without optional values', () => {
        const activity = new Activity(0,1,ActivityType.WALKING);
        expect(activity.getActivity()).to.equal(ActivityType.WALKING);
    })

    it('Activity to MeasurementEvent', () => {
        const activity = new Activity(0,1,ActivityType.WALKING,ActivityConfidence.HIGH);
        const measurementEvent : MeasurementEvent = activity;
        expect(measurementEvent.getStartTime()).to.equal(activity.getStartTime());
        expect(measurementEvent.getEndTime()).to.equal(activity.getEndTime());
        expect((measurementEvent as Activity).getActivity()).to.equal(activity.getActivity());
        expect((measurementEvent as Activity).getConfidence()).to.equal(activity.getConfidence());
    })
} )