import {expect} from 'chai'
import HeartRate from 'src/heart-rate'
import { MeasurementEvent } from 'src/measurement-event'

describe('HeartRate', () => {

    it('Create HeartRate', () => {
        const heartRate = new HeartRate(0,1,45,42,56);
        expect(heartRate.getAverageHeartRate()).to.equal(45);
        expect(heartRate.getMinimumHeartRate()).to.equal(42);
        expect(heartRate.getMaximumHeartRate()).to.equal(56);
        expect(heartRate.getTime()).to.equal(undefined);
        expect(heartRate.getStartTime()).to.equal(0);
        expect(heartRate.getEndTime()).to.equal(1);
    })
    it('Create heartRate without optional values', () => {
        const heartRate = new HeartRate(0,1,45);
        expect(heartRate.getAverageHeartRate()).to.equal(45);
    })

    it('heartRate to MeasurementEvent', () => {
        const heartRate = new HeartRate(0,1,45,42,56);
        const measurementEvent : MeasurementEvent = heartRate;
        expect(measurementEvent.getStartTime()).to.equal(heartRate.getStartTime());
        expect(measurementEvent.getEndTime()).to.equal(heartRate.getEndTime());
        expect((measurementEvent as HeartRate).getAverageHeartRate()).to.equal(heartRate.getAverageHeartRate());
        expect((measurementEvent as HeartRate).getMaximumHeartRate()).to.equal(heartRate.getMaximumHeartRate());
        expect((measurementEvent as HeartRate).getMinimumHeartRate()).to.equal(heartRate.getMinimumHeartRate());
    })
} )