import { expect } from 'chai'
import { MeasurementEvent, MeasurementEventType } from 'src/measurement-event'

describe('Measurement Event', () => {

    it('Create measurement event with time', () => {
        const measurementEvent = new MeasurementEvent(MeasurementEventType.GPSLOCATION, 1587219967609);
        expect(measurementEvent.getType()).to.equal(MeasurementEventType.GPSLOCATION);
        expect(measurementEvent.getTime()).to.equal(1587219967609);
    })

    it('Create measurement event with start time and end time', () => {
        const measurementEvent = new MeasurementEvent(MeasurementEventType.STEPCOUNT, undefined, 1587065096451, 1587065156451);
        expect(measurementEvent.getType()).to.equal(MeasurementEventType.STEPCOUNT);
        expect(measurementEvent.getStartTime()).to.equal(1587065096451);
        expect(measurementEvent.getEndTime()).to.equal(1587065156451);
    })

    it('Create measurement event with negative time', () => {
        expect(() => new MeasurementEvent(MeasurementEventType.GPSLOCATION, -1)).to.throw()
    })

    it('Create measurement event with negative startTime', () => {
        expect(() => new MeasurementEvent(MeasurementEventType.STEPCOUNT, undefined, -1, 0)).to.throw()
    })

    it('Create measurement event with negative endTime', () => {
        expect(() => new MeasurementEvent(MeasurementEventType.STEPCOUNT, undefined, 2, -1)).to.throw()
    })

    it('Create measurement event with startTime greater than endTime', () => {
        expect(() => new MeasurementEvent(MeasurementEventType.STEPCOUNT, undefined, 4, 2)).to.throw()
    })
})