import {expect} from 'chai'
import StepCount from 'src/step-count'
import { MeasurementEvent } from 'src/measurement-event'

describe('StepCount', () => {

    it('Create stepCount', () => {
        const stepCount = new StepCount(23,46,0);
        expect(stepCount.getStepCount()).to.equal(0);
        expect(stepCount.getTime()).to.equal(undefined);
        expect(stepCount.getStartTime()).to.equal(23);
        expect(stepCount.getEndTime()).to.equal(46);
    })

    it('StepCount to MeasurementEvent', () => {
        const stepCount = new StepCount(23,46,1);
        const measurementEvent : MeasurementEvent = stepCount;
        expect(stepCount.getStartTime()).to.equal(measurementEvent.getStartTime());
        expect(stepCount.getEndTime()).to.equal(measurementEvent.getEndTime());
        expect((measurementEvent as StepCount).getStepCount()).to.equal(stepCount.getStepCount());
    })
} )