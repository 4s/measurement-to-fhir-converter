import { expect } from 'chai'
import MeasurementToFHIRConverter, { ActivityType, ActivityConfidence } from 'src/measurement-to-fhir-converter'

describe('Measurement To FHIR Converter', () => {
    let deviceId: string
    let patientId: string
    let carePlanId: string
    let measurementToFHIRConverter: MeasurementToFHIRConverter

    beforeEach(() => {
        deviceId = "114D9920-BB51-4C3B-BF05-81679A2477B0";
        patientId = "2512489996";
        carePlanId = "de67e5c1-494a-41b9-924d-4c613b076a73";
        measurementToFHIRConverter = new MeasurementToFHIRConverter(deviceId, patientId, carePlanId);
    })
    it('Create Bundle for StepCount', () => {
        const stepCountBundle = measurementToFHIRConverter.convertStepCountEvent(1587065096451, 1587065156451, 7);
        expect(stepCountBundle.entry.length).to.equal(3);
    })
    it('Create Bundle for GPS Location', () => {
        const gpsLocationBundle = measurementToFHIRConverter.convertGPSLocationEvent(1587137643779, 23, 47, 2, 4);
        expect(gpsLocationBundle.entry.length).to.equal(3);
    })
    it('Create Bundle for Activity', () => {
        const activityBundle = measurementToFHIRConverter.convertActivityEvent(1587365564362, 1587366433675, ActivityType.CYCLING, ActivityConfidence.HIGH);
        expect(activityBundle.entry.length).to.equal(3);
    })
    it('Create Bundle for HeartRate', () => {
        const heartRateBundle = measurementToFHIRConverter.convertHeartRateEvent(1587065096451, 1587065156451, 72);
        expect(heartRateBundle.entry.length).to.equal(3);
    })
    it('Create Multiple Bundles for Different Measurement Types', () => {
        const stepCountBundle = measurementToFHIRConverter.convertStepCountEvent(1587065096451, 1587065156451, 7);
        const gpsLocationBundle = measurementToFHIRConverter.convertGPSLocationEvent(1587137643779, 23, 47);
        const activityBundle = measurementToFHIRConverter.convertActivityEvent(1587365564362, 1587366433675, ActivityType.CYCLING, ActivityConfidence.HIGH);
        const heartRateBundle = measurementToFHIRConverter.convertHeartRateEvent(1587065096451, 1587065156451, 72);
        expect(stepCountBundle.entry.length).to.equal(3);
        expect(gpsLocationBundle.entry.length).to.equal(3);
        expect(activityBundle.entry.length).to.equal(3);
        expect(heartRateBundle.entry.length).to.equal(3);
    })
    it('Create Multiple Bundles for Same Measurement Type', () => {
        const stepCountBundle1 = measurementToFHIRConverter.convertStepCountEvent(0, 0, 1);
        const stepCountBundle2 = measurementToFHIRConverter.convertStepCountEvent(0, 1, 1);
        expect(stepCountBundle1.entry.length).to.equal(3);
        expect(stepCountBundle2.entry.length).to.equal(3);
    })
    it('Package Multiple Events To Single Bundle', () => {
        measurementToFHIRConverter.addStepCountEvent(0, 1, 1);
        measurementToFHIRConverter.addGPSLocationEvent(0, 23, 47);
        measurementToFHIRConverter.addActivityEvent(0, 1, ActivityType.CYCLING, ActivityConfidence.HIGH);
        measurementToFHIRConverter.addHeartRateEvent(0, 1, 56);
        const finalBundle = measurementToFHIRConverter.bundleAddedEvents();
        expect(finalBundle.entry.length).to.equal(6);
    })
    it('Package Multiple Events To Single Bundle Chained', () => {
        const finalBundle = measurementToFHIRConverter.addStepCountEvent(0, 1, 1)
            .addGPSLocationEvent(0, 23, 47)
            .addActivityEvent(0, 1, ActivityType.CYCLING, ActivityConfidence.HIGH)
            .addHeartRateEvent(0, 1, 56)
            .bundleAddedEvents();
        expect(finalBundle.entry.length).to.equal(6);
    })
    it('Clear Added Events', () => {
        measurementToFHIRConverter.addStepCountEvent(0, 1, 1);
        measurementToFHIRConverter.addGPSLocationEvent(0, 23, 47);
        measurementToFHIRConverter.clearAddedEvents();
        measurementToFHIRConverter.addActivityEvent(0, 1, ActivityType.CYCLING, ActivityConfidence.HIGH);
        measurementToFHIRConverter.addHeartRateEvent(0, 1, 56);
        const finalBundle = measurementToFHIRConverter.bundleAddedEvents();
        expect(finalBundle.entry.length).to.equal(4);
    })
    it('Pop Last Added Event', () => {
        measurementToFHIRConverter.addStepCountEvent(0, 1, 1);
        measurementToFHIRConverter.addGPSLocationEvent(0, 23, 47);
        measurementToFHIRConverter.popLastAddedEvent();
        measurementToFHIRConverter.addActivityEvent(0, 1, ActivityType.CYCLING, ActivityConfidence.HIGH);
        measurementToFHIRConverter.addHeartRateEvent(0, 1, 56);
        const finalBundle = measurementToFHIRConverter.bundleAddedEvents();
        expect(finalBundle.entry.length).to.equal(5);
    })
})