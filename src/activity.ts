import { MeasurementEvent, MeasurementEventType } from "./measurement-event";

/**
 * Enumeration for different types of activity.
 */
export enum ActivityType {

    /**
     * The user is stationary.
     */
    STATIONARY = "STATIONARY",
    /**
     * The user is walking.
     */
    WALKING = "WALKING",
    /**
     * The user is running.
     */
    RUNNING = "RUNNING",
    /**
     * The user is in automotive transport.
     */
    AUTOMOTIVE = "AUTOMOTIVE",
    /**
     * The user is cycling.
     */
    CYCLING = "CYCLING",
    /**
     * The activity of the user is unknown.
     */
    UNKNOWN = "UNKNOWN"
}

/**
 * Enumeration for different levels of confidence in measurement of activity.
 */
export enum ActivityConfidence {
    /**
     * The confidence of measurement is low.
     */
    LOW = "LOW",
    /**
     * The confidence of measurement is medium.
     */
    MEDIUM = "MEDIUM",
    /**
     * The confidence of measurement is high.
     */
    HIGH = "HIGH"
}

/**
 * Class representing measurement of activity.
 */
export class Activity extends MeasurementEvent {
    /**
     * The activity type measured.
     * @private
     * @type {ActivityType}
     * @ignore
     */
    private activity: ActivityType;
    /**
     * The confidence of measurement of activity type.
     * @private
     * @type {ActivityConfidence}
     * @ignore
     */
    private confidence: ActivityConfidence;

    /**
     * Create an Activity measurementEvent.
     * @param startTime Start time of the measurement in unix timestamp (milliseconds) format.
     * @param endTime  End time of the measurement in unix timestamp (milliseconds) format.
     * @param activity Type of activity.
     * @param confidence Confidence of measurement.
     */
    constructor(startTime: number, endTime: number, activity: ActivityType, confidence?: ActivityConfidence) {
        super(MeasurementEventType.ACTIVITY, undefined, startTime, endTime);
        this.activity = activity;
        this.confidence = confidence;
    }

    /**
     * Get the activity type.
     * @returns Type of Activity
     */
    public getActivity(): ActivityType {
        return this.activity;
    }
    /**
     * Get the confidence of measurement of activity type.
     * @returns Confidence of measurement of activity type.
     */
    public getConfidence(): ActivityConfidence {
        return this.confidence;
    }
}