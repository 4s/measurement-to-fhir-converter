import { MeasurementEvent } from "./measurement-event";
/**
 * Class representing measurement of type stepCount.
 */
export default class StepCount extends MeasurementEvent {
    /**
     * The number of steps.
     * @private
     * @type {number}
     * @ignore
     */
    private stepCount;
    /**
     * Create a StepCount MeasurementEvent
     * @param startTime Start time of the measurement in unix timestamp (milliseconds) format.
     * @param endTime  End time of the measurement in unix timestamp (milliseconds) format.
     * @param stepCount The number of steps measured in the period of measurement.
     */
    constructor(startTime: number, endTime: number, stepCount: number);
    /**
     * Get the stepCount.
     * @returns The stepCount.
     */
    getStepCount(): number;
}
