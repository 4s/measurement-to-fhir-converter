import DeviceMeasurementEngine from "device-measurement-engine"
import PackagingEngine from "packaging-engine"
import { Device, Patient, Bundle, Observation } from "fhir"
import { MeasurementEvent, MeasurementEventType } from "./measurement-event"
import HeartRate from "./heart-rate";
import GPSLocation from "./gps-location";
import StepCount from "./step-count";
import { Activity, ActivityType, ActivityConfidence } from "./activity";

/** Class for MeasurementToFHIRConverter */
export default class MeasurementToFHIRConverter {

    /**
     * The ID of the device.
     * @private
     * @type {string}
     * @ignore
     */
    private deviceId: string;

    /**
     * The ID of the patient.
     * @private
     * @type {string}
     * @ignore
     */
    private patientId: string


    /**
     * The ID of careplan of patient.
     * @private
     * @type {string}
     * @ignore
     */
    private carePlanId: string;


    /**
     * The FHIR representation of the device.
     * @private
     * @type { Device }
     * @ignore
     */
    private device: Device;

    /**
     * The FHIR representation of user using the healthcare app.
     * @private
     * @type { Patient }
     * @ignore
     */
    private patient: Patient;

    /**
     * Packaging Engine to build FHIR Observation, CodeableConcept, Device and Identifier.
     * @private
     * @type {PackagingEngine}
     * @ignore
     */
    private deviceMeasurementEngine: DeviceMeasurementEngine;

    /**
     * System of identifier of the device.
     * @private
     * @type {string}
     * @ignore
     */
    private DEVICE_IDENTIFIER_SYSTEM = "urn:oid:1.2.840.10004.1.1.1.0.0.1.0.0.1.2680";

    /**
     * System of identifier of the patient.
     * @private
     * @type {string}
     * @ignore
     */
    private PATIENT_IDENTIFIER_SYSTEM = "urn:oid:1.2.208.176.1.2";

    /**
     * System of identifier of the observations.
     * @private
     * @type {string}
     * @ignore
     */
    private OBSERVATION_IDENTIFIER_SYSTEM = "urn:ietf:rfc:3986";

    /**
     * System of identifier of the careplan.
     * @private
     * @type {string}
     * @ignore
     */
    private CAREPLAN_IDENTIFIER_SYSTEM = "urn:ietf:rfc:3986";

    /**
     * System of IEEE Personal Health Device Observations.
     * @private
     * @type {string}
     * @ignore
     */
    private HF_IEEE_SYSTEM = "urn:iso:std:iso:11073:10101";

    /**
     * List of events to be Bundled together.
     * @private
     * @type {MeasurementEvent[]}
     * @ignore
     */
    private events: MeasurementEvent[];

    /**
     * Create FHIR Data Handler given the deviceId, patientId and carePlanId.
     * @param deviceId The ID of device.
     * @param patientId The ID of patient.
     * @param careplanId The ID of careplan of patient.
     * @param deviceIdentifierSystem The system of device identifier.
     * @param patientIdentifierSystem The system of patient identifier.
     * @param carePlanIdentifierSystem The system of carePlan identifier.
     */
    public constructor(deviceId: string, patientId: string, careplanId: string, deviceIdentifierSystem?: string, patientIdentifierSystem?: string, carePlanIdentifierSystem?: string) {
        this.deviceMeasurementEngine = new DeviceMeasurementEngine();
        this.patientId = patientId;
        this.deviceId = deviceId;
        this.device = this.createDevice();
        this.device = this.setDevicePUTId();
        this.patient = this.createPatient();
        this.carePlanId = careplanId;
        if (!(deviceIdentifierSystem == null)) this.DEVICE_IDENTIFIER_SYSTEM = deviceIdentifierSystem;
        if (!(patientIdentifierSystem == null)) this.PATIENT_IDENTIFIER_SYSTEM = patientIdentifierSystem;
        if (!(carePlanIdentifierSystem == null)) this.CAREPLAN_IDENTIFIER_SYSTEM = carePlanIdentifierSystem;
        this.events = [];
    }

    /**
     * Create and return a FHIR Device with current deviceId.
     * @returns FHIR Device with current deviceId.
     */

    private createDevice(): Device {

        const deviceBuilder = this.deviceMeasurementEngine.getDeviceBuilder();
        const deviceIdentifierBuilder = this.deviceMeasurementEngine.getIdentifierBuilder();
        deviceIdentifierBuilder.setSystem(this.DEVICE_IDENTIFIER_SYSTEM);
        deviceIdentifierBuilder.setValue(this.deviceId);
        deviceIdentifierBuilder.setType(undefined, "http://hl7.org/fhir/uv/phd/CodeSystem/ContinuaDeviceIdentifiers", undefined, "SYSID");
        deviceBuilder.setIdentifier(deviceIdentifierBuilder.build());


        const deviceTypeCodeableConceptBuilder = this.deviceMeasurementEngine.getCodeableConceptBuilder();
        deviceTypeCodeableConceptBuilder.addCoding(this.HF_IEEE_SYSTEM, undefined, "65573", "MDC_MOC_VMS_MDS_SIMP", undefined);
        deviceBuilder.setType(deviceTypeCodeableConceptBuilder.build());

        const deviceVersionCodeableConceptBuilder = this.deviceMeasurementEngine.getCodeableConceptBuilder();
        deviceVersionCodeableConceptBuilder.addCoding(this.HF_IEEE_SYSTEM, undefined, "532352", "MDC_REG_CERT_DATA_CONTINUA_VERSION", false);
        deviceVersionCodeableConceptBuilder.setText("Continua Design Guidelines version");
        deviceBuilder.addVersion(deviceVersionCodeableConceptBuilder.build(), undefined, "07.00");


        const devicePropertyBuilder = this.deviceMeasurementEngine.getDevicePropertyBuilder()

        const deviceRegulationTypeCodeableConceptBuilder = this.deviceMeasurementEngine.getCodeableConceptBuilder();
        deviceRegulationTypeCodeableConceptBuilder.addCoding("http://hl7.org/fhir/uv/phd/CodeSystem/ASN1ToHL7", undefined, "532354.0", "regulation-status")
        deviceRegulationTypeCodeableConceptBuilder.setText("Regulation status")
        devicePropertyBuilder.setType(deviceRegulationTypeCodeableConceptBuilder.build());

        const deviceRegulationValueCodeableConceptBuilder = this.deviceMeasurementEngine.getCodeableConceptBuilder();
        deviceRegulationValueCodeableConceptBuilder.addCoding("http://terminology.hl7.org/CodeSystem/v2-0136", undefined, "Y", "unregulated", false);
        deviceRegulationValueCodeableConceptBuilder.setText("Continua Design Guidelines version");
        devicePropertyBuilder.addValueCode(deviceRegulationValueCodeableConceptBuilder.build());
        deviceBuilder.addProperty(devicePropertyBuilder.build());

        const deviceSpecializationCodeableConceptBuilder = this.deviceMeasurementEngine.getCodeableConceptBuilder();
        deviceSpecializationCodeableConceptBuilder.addCoding(this.HF_IEEE_SYSTEM, undefined, '528425', 'MDC_DEV_SPEC_PROFILE_HF_CARDIO', undefined);
        deviceSpecializationCodeableConceptBuilder.setText('MDC_DEV_SPEC_PROFILE_HF_CARDIO: Cardiovascular Fitness and Activity Monitor');
        deviceBuilder.addSpecialization(deviceSpecializationCodeableConceptBuilder.build(), '1');

        deviceBuilder.setManufacturer("Apple Inc");
        deviceBuilder.setModelNumber("MC918");

        return deviceBuilder.build();
    }

    /**
     * Set the PUT id for current device and returns it.
     * @returns The current device with id attribute set as current deviceId.
     */
    private setDevicePUTId(): Device {
        const device = this.device;
        device.id = this.deviceId;
        return device;
    }
    /**
     * Create and return a FHIR Patient with current patientId.
     * @returns FHIR Patient with current patientId.
     */
    private createPatient(): Patient {
        const packagingEngine = new PackagingEngine();
        const patientBuilder = packagingEngine.getPatientBuilder();
        const patientIdentifierBuilder = packagingEngine.getIdentifierBuilder();
        patientIdentifierBuilder.setSystem(this.PATIENT_IDENTIFIER_SYSTEM);
        patientIdentifierBuilder.setValue(this.patientId);
        patientIdentifierBuilder.setType(undefined, "http://terminology.hl7.org/CodeSystem/v2-0203", undefined, "NNDNK");
        patientBuilder.setIdentifier(patientIdentifierBuilder.build());
        return patientBuilder.build();
    }

    /**
     * Return the patient.
     * @returns Current patient.
     */
    public getPatient(): Patient {
        return this.patient;
    }

    /**
     * Return the device
     * @returns Current device.
     */
    public getDevice(): Device {
        return this.device;
    }

    /**
     * Return the patient ID.
     * @returns Current patientId.
     */
    public getPatientId(): string {
        return this.patientId;
    }

    /**
     * Return the device ID.
     * @returns Current deviceId.
     */
    public getDeviceId(): string {
        return this.deviceId;
    }

    /**
     * Return the careplan ID.
     * @returns Current carePlanId.
     */
    public getCarePlanId(): string {
        return this.carePlanId;
    }

    /**
     * Convert a standard event to a FHIR Observation.
     * @param event standard event to be converted to observation.
     * @returns FHIR Observation of standard event.
     */
    private standardEventToFHIR(event: MeasurementEvent): Observation {

        const observationBuilder = this.deviceMeasurementEngine.getObservationBuilder();

        const carePlanIdentifierBuilder = this.deviceMeasurementEngine.getIdentifierBuilder();
        carePlanIdentifierBuilder.setSystem(this.CAREPLAN_IDENTIFIER_SYSTEM);
        carePlanIdentifierBuilder.setValue(this.getCarePlanId());
        observationBuilder.setBasedOn(undefined, carePlanIdentifierBuilder.build());

        observationBuilder.setStatus("final");

        observationBuilder.setSubject("urn:uuid:" + this.getPatientId());
        observationBuilder.setPerformer("urn:uuid:" + this.getPatientId());

        observationBuilder.setDevice(this.getDeviceId());

        if (!(event.getTime() == null)) {
            observationBuilder.setEffectiveDateTime((new Date(event.getTime())).toISOString());
        }
        else {
            observationBuilder.setEffectivePeriod((new Date(event.getStartTime())).toISOString(), (new Date(event.getEndTime())).toISOString());
        }

        switch (event.getType()) {
            case MeasurementEventType.STEPCOUNT:
                observationBuilder.setCode('MDC_HF_DISTANCE: Step Count', this.HF_IEEE_SYSTEM, undefined, '8454247', 'MDC_HF_DISTANCE', undefined);
                observationBuilder.setValueQuantity((event as StepCount).getStepCount(), undefined, 'steps', 'http://unitsofmeasure.org', '{steps}');

                break;
            case MeasurementEventType.GPSLOCATION:
                observationBuilder.setCode('MDC_ATTR_GPS_COORDINATES: GPS Coordinates', this.HF_IEEE_SYSTEM, undefined, '68532', 'MDC_ATTR_GPS_COORDINATES', undefined);

                const longitudeComponentBuilder = this.deviceMeasurementEngine.getObservationComponentBuilder();
                longitudeComponentBuilder.setCode('MDC_HF_LONGITUDE: Longitude', this.HF_IEEE_SYSTEM, undefined, '8454251', 'MDC_HF_LONGITUDE', undefined);
                longitudeComponentBuilder.setValueQuantity((event as GPSLocation).getLongitude(), undefined, 'angle degree', 'http://unitsofmeasure.org', 'deg');
                observationBuilder.addComponent(longitudeComponentBuilder.build());


                const latitudeComponentBuilder = this.deviceMeasurementEngine.getObservationComponentBuilder();
                latitudeComponentBuilder.setCode('MDC_HF_LATITUDE: Latitude', this.HF_IEEE_SYSTEM, undefined, '8454250', 'MDC_HF_LATITUDE', undefined);
                latitudeComponentBuilder.setValueQuantity((event as GPSLocation).getLatitude(), undefined, 'angle degree', 'http://unitsofmeasure.org', 'deg');
                observationBuilder.addComponent(latitudeComponentBuilder.build());

                if (!((event as GPSLocation).getAltitude() == null)) {
                    const altitudeComponentBuilder = this.deviceMeasurementEngine.getObservationComponentBuilder();
                    altitudeComponentBuilder.setCode('MDC_HF_ALT: Altitude', this.HF_IEEE_SYSTEM, undefined, '8454246', 'MDC_HF_ALT', undefined);
                    altitudeComponentBuilder.setValueQuantity((event as GPSLocation).getAltitude(), undefined, 'meter', 'http://unitsofmeasure.org', 'm');
                    observationBuilder.addComponent(altitudeComponentBuilder.build());
                }

                if (!((event as GPSLocation).getHorizontalAccuracy() == null)) {
                    const latitudeAccuracyComponentBuilder = this.deviceMeasurementEngine.getObservationComponentBuilder();
                    latitudeAccuracyComponentBuilder.setCode('MDC_ATTR_GPS_COORD_ACCY: GPS Accuracy', this.HF_IEEE_SYSTEM, undefined, '68535', 'MDC_ATTR_GPS_COORD_ACCY', undefined);
                    latitudeAccuracyComponentBuilder.setValueQuantity((event as GPSLocation).getHorizontalAccuracy(), undefined, 'angle degree', 'http://unitsofmeasure.org', 'deg');
                    observationBuilder.addComponent(latitudeAccuracyComponentBuilder.build());
                }
                break;
            case MeasurementEventType.ACTIVITY:
                observationBuilder.setCode('MDC_HF_SESSION: Acitivity Type', this.HF_IEEE_SYSTEM, undefined, '8454267', 'MDC_HF_SESSION', undefined);
                switch ((event as Activity).getActivity()) {
                    case ActivityType.RUNNING:
                        observationBuilder.setValueCodeableConcept('MDC_HF_ACT_RUN: Person is running', this.HF_IEEE_SYSTEM, undefined, '8455155', 'MDC_HF_ACT_RUN');
                        break;
                    case ActivityType.WALKING:
                        observationBuilder.setValueCodeableConcept('MDC_HF_ACT_WALK: Person is walking', this.HF_IEEE_SYSTEM, undefined, '8455161', 'MDC_HF_ACT_WALK');
                        break;
                    case ActivityType.STATIONARY:
                        observationBuilder.setValueCodeableConcept('MDC_HF_ACT_REST: Person is at rest', this.HF_IEEE_SYSTEM, undefined, '8455145', 'MDC_HF_ACT_REST');
                        break;
                    case ActivityType.CYCLING:
                        observationBuilder.setValueCodeableConcept('MDC_HF_ACT_BIKE: Person is cycling', this.HF_IEEE_SYSTEM, undefined, '8455156', 'MDC_HF_ACT_BIKE');
                        break;
                    case ActivityType.AUTOMOTIVE:
                        observationBuilder.setValueCodeableConcept('MDC_HF_ACT_MOTOR: Person is in motorized transportation', this.HF_IEEE_SYSTEM, undefined, '8455146', 'MDC_HF_ACT_MOTOR');
                        break;
                    case ActivityType.UNKNOWN:
                        observationBuilder.setValueCodeableConcept('MDC_HF_ACT_UNKNOWN: Activity is unknown', this.HF_IEEE_SYSTEM, undefined, '8455151', 'MDC_HF_ACT_UNKNOWN');
                        break;
                    default:
                        throw new Error('Activity type not recognized');
                }
                break;
            case MeasurementEventType.HEARTRATE:
                observationBuilder.setCode('MDC_HF_HR: Heart Rate', this.HF_IEEE_SYSTEM, undefined, '8454258', 'MDC_HF_HR', undefined);
                observationBuilder.setValueQuantity((event as HeartRate).getAverageHeartRate(), undefined, 'steps', 'http://unitsofmeasure.org', '{steps}');
                break;
            default:
                throw new Error('Measurement type not recognized');
        }

        return observationBuilder.build();
    }

    /**
     * Package a list of FHIR Observations to a FHIR Bundle.
     * @param observations List of FHIR Observations to be packaged.
     * @returns FHIR Bundle with current patient, passed observation and current device.
     */
    private packageObservations(observations: Observation[]): Bundle {
        const packagingEngine = new PackagingEngine();

        packagingEngine
            .setPackageType("transaction")
            .addRequestEntry(
                packagingEngine
                    .getEntryBuilder()
                    .setResource(this.device)
                    .setMethod("PUT")
                    .setFullUrl(this.deviceId)
                    .build()
            )
            .addRequestEntry(
                packagingEngine
                    .getEntryBuilder()
                    .setMethod("POST")
                    .setFullUrl("urn:uuid:" + this.patientId)
                    .setRequestCondition(
                        "ifNoneExist",
                        "identifier=" +
                        this.PATIENT_IDENTIFIER_SYSTEM + "|" +
                        this.patientId
                    )
                    .setResource(this.patient)
                    .build()
            )

        observations.forEach(observation => {
            const observationIdentifierValue = observation.identifier[0].value;
            packagingEngine.addRequestEntry(
                packagingEngine
                    .getEntryBuilder()
                    .setMethod("POST")
                    .setRequestCondition(
                        "ifNoneExist",
                        "identifier=" +
                        this.OBSERVATION_IDENTIFIER_SYSTEM +
                        "|" +
                        observationIdentifierValue
                    )
                    .setResource(observation)
                    .build()
            )
        })


        return packagingEngine.package();
    }

    /**
     * Create a FHIR Bundle from Activity measurement.
     * @param startTime Start time of the measurement in unix timestamp (milliseconds) format.
     * @param endTime  End time of the measurement in unix timestamp (milliseconds) format.
     * @param activity Type of activity.
     * @param confidence Confidence of measurement.
     * @returns FHIR Bundle consisting of current patient, Activity as FHIR observation and current device.
     */
    public convertActivityEvent(startTime: number, endTime: number, activity: ActivityType, confidence?: ActivityConfidence): Bundle {
        const activityEvent = new Activity(startTime, endTime, activity, confidence);
        const activityEventObservation = this.standardEventToFHIR(activityEvent);
        return this.packageObservations([activityEventObservation]);
    }

    /**
     * Create a FHIR Bundle from GPS Location measurement.
     * @param time The time of measurement in unix timestamp (milliseconds) format.
     * @param latitude The latitude of GPS measurement.
     * @param longitude The longitude of GPS measurement.
     * @param altitude The altitude of GPS measurement.
     * @param horizontalAccuracy The horizontal accuracy of the measurement.
     * @returns FHIR Bundle consisting of current patient, GPS Location as FHIR observation and current device.
     */
    public convertGPSLocationEvent(time: number, latitude: number, longitude: number, altitude?: number, horizontalAccuracy?: number): Bundle {
        const gpsLocationEvent = new GPSLocation(time, latitude, longitude, altitude, horizontalAccuracy);
        const gpsLocationEventObservation = this.standardEventToFHIR(gpsLocationEvent);
        return this.packageObservations([gpsLocationEventObservation]);
    }

    /**
     * Create a FHIR Bundle from HeartRate measurement.
     * @param startTime Start time of the measurement in unix timestamp (milliseconds) format.
     * @param endTime  End time of the measurement in unix timestamp (milliseconds) format.
     * @param averageHeartRate Average heartRate over the measurement period.
     * @param minimumHeartRate Minimum heartRate over the measurement period.
     * @param maximumHeartRate Maximum heartRate over the measurement period.
     * @returns FHIR Bundle consisting of current patient, HeartRate as FHIR observation and current device.
     */
    public convertHeartRateEvent(startTime: number, endTime: number, averageHeartRate: number, minimumHeartRate?: number, maximumHeartRate?: number): Bundle {
        const heartRateEvent = new HeartRate(startTime, endTime, averageHeartRate, minimumHeartRate, maximumHeartRate);
        const heartRateEventObservation = this.standardEventToFHIR(heartRateEvent);
        return this.packageObservations([heartRateEventObservation]);
    }

    /**
     * Create a FHIR Bundle from StepCount measurement.
     * @param startTime Start time of the measurement in unix timestamp (milliseconds) format.
     * @param endTime  End time of the measurement in unix timestamp (milliseconds) format.
     * @param stepCount The number of steps measured in the period of measurement.
     * @returns FHIR Bundle consisting of current patient, StepCount as FHIR observation and current device. 
     */
    public convertStepCountEvent(startTime: number, endTime: number, stepCount: number): Bundle {
        const stepCountEvent = new StepCount(startTime, endTime, stepCount);
        const stepCountEventObservation = this.standardEventToFHIR(stepCountEvent);
        return this.packageObservations([stepCountEventObservation]);
    }

    /**
     * Add events to the event list to be bundled into a single FHIR Bundle.
     * @param event The event to be added to the event list.
     * @returns The updated MeasurementToFHIRConverter object.
     */
    private addEvent(event: MeasurementEvent): MeasurementToFHIRConverter {
        if (Array.isArray(this.events)) {
            this.events.push(event)
        }
        return this;
    }

    /**
     * Get the events currently added to the MeasurementToFHIRConverter Object.
     * @returns An array of added events.
     */
    public getEvents(): MeasurementEvent[] {
        return this.events;
    }

    /**
     * Clears the currently added events to the MeasurementToFHIRConverter Object.
     */
    public clearAddedEvents(): void {
        this.events = []
    }

    /**
     * Pops the last added event.
     */
    public popLastAddedEvent(): MeasurementEvent {
        return this.events.pop();
    }

    /**
     * Add a measurement of type Activity to the event list of he MeasurementToFHIRConverter Object.
     * @param startTime Start time of the measurement in unix timestamp (milliseconds) format.
     * @param endTime  End time of the measurement in unix timestamp (milliseconds) format.
     * @param activity Type of activity.
     * @param confidence Confidence of measurement.
     * @returns The updated MeasurementToFHIRObject.
     */
    public addActivityEvent(startTime: number, endTime: number, activity: ActivityType, confidence?: ActivityConfidence): MeasurementToFHIRConverter {
        const activityEvent = new Activity(startTime, endTime, activity, confidence);
        return this.addEvent(activityEvent);
    }

    /**
     * Add a measurement of type GPSLocation to the event list of he MeasurementToFHIRConverter Object.
     * @param time The time of measurement in unix timestamp (milliseconds) format.
     * @param latitude The latitude of GPS measurement.
     * @param longitude The longitude of GPS measurement.
     * @param altitude The altitude of GPS measurement.
     * @param horizontalAccuracy The horizontal accuracy of the measurement.
     * @returns The updated MeasurementToFHIRObject.
     */
    public addGPSLocationEvent(time: number, latitude: number, longitude: number, altitude?: number, horizontalAccuracy?: number): MeasurementToFHIRConverter {
        const gpsLocationEvent = new GPSLocation(time, latitude, longitude, altitude, horizontalAccuracy);
        return this.addEvent(gpsLocationEvent);
    }


    /**
     * Add a measurement of type HeartRate to the event list of he MeasurementToFHIRConverter Object.
     * @param startTime Start time of the measurement in unix timestamp (milliseconds) format.
     * @param endTime  End time of the measurement in unix timestamp (milliseconds) format.
     * @param averageHeartRate Average heartRate over the measurement period.
     * @param minimumHeartRate Minimum heartRate over the measurement period.
     * @param maximumHeartRate Maximum heartRate over the measurement period.
     * @returns The updated MeasurementToFHIRObject.
     */
    public addHeartRateEvent(startTime: number, endTime: number, averageHeartRate: number, minimumHeartRate?: number, maximumHeartRate?: number): MeasurementToFHIRConverter {
        const heartRateEvent = new HeartRate(startTime, endTime, averageHeartRate, minimumHeartRate, maximumHeartRate);
        return this.addEvent(heartRateEvent);
    }

    /**
     * Add a measurement of type StepCount to the event list of he MeasurementToFHIRConverter Object.
     * @param startTime Start time of the measurement in unix timestamp (milliseconds) format.
     * @param endTime  End time of the measurement in unix timestamp (milliseconds) format.
     * @param stepCount The number of steps measured in the period of measurement.
     * @returns The updated MeasurementToFHIRObject.
     */
    public addStepCountEvent(startTime: number, endTime: number, stepCount: number): MeasurementToFHIRConverter {
        const stepCountEvent = new StepCount(startTime, endTime, stepCount);
        return this.addEvent(stepCountEvent);
    }

    /**
     * Bundles the added events to a FHIR Bundle and clears them from the MeasurementToFHIRObject.
     */
    public bundleAddedEvents(): Bundle {

        if (!Array.isArray(this.events) || !this.events.length) {
            throw new Error('No added events, event list is empty.');
        }
        const observations: Observation[] = [];
        this.events.forEach(event => {
            const observation = this.standardEventToFHIR(event);
            observations.push(observation)
        })

        this.clearAddedEvents();

        return this.packageObservations(observations);
    }


}
export { ActivityType, ActivityConfidence }