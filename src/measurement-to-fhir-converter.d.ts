import { Device, Patient, Bundle } from "fhir";
import { MeasurementEvent } from "./measurement-event";
import { ActivityType, ActivityConfidence } from "./activity";
/** Class for MeasurementToFHIRConverter */
export default class MeasurementToFHIRConverter {
    /**
     * The ID of the device.
     * @private
     * @type {string}
     * @ignore
     */
    private deviceId;
    /**
     * The ID of the patient.
     * @private
     * @type {string}
     * @ignore
     */
    private patientId;
    /**
     * The ID of careplan of patient.
     * @private
     * @type {string}
     * @ignore
     */
    private carePlanId;
    /**
     * The FHIR representation of the device.
     * @private
     * @type { Device }
     * @ignore
     */
    private device;
    /**
     * The FHIR representation of user using the healthcare app.
     * @private
     * @type { Patient }
     * @ignore
     */
    private patient;
    /**
     * Packaging Engine to build FHIR Observation, CodeableConcept, Device and Identifier.
     * @private
     * @type {PackagingEngine}
     * @ignore
     */
    private deviceMeasurementEngine;
    /**
     * System of identifier of the device.
     * @private
     * @type {string}
     * @ignore
     */
    private DEVICE_IDENTIFIER_SYSTEM;
    /**
     * System of identifier of the patient.
     * @private
     * @type {string}
     * @ignore
     */
    private PATIENT_IDENTIFIER_SYSTEM;
    /**
     * System of identifier of the observations.
     * @private
     * @type {string}
     * @ignore
     */
    private OBSERVATION_IDENTIFIER_SYSTEM;
    /**
     * System of identifier of the careplan.
     * @private
     * @type {string}
     * @ignore
     */
    private CAREPLAN_IDENTIFIER_SYSTEM;
    /**
     * System of IEEE Personal Health Device Observations.
     * @private
     * @type {string}
     * @ignore
     */
    private HF_IEEE_SYSTEM;
    /**
     * List of events to be Bundled together.
     * @private
     * @type {MeasurementEvent[]}
     * @ignore
     */
    private events;
    /**
     * Create FHIR Data Handler given the deviceId, patientId and carePlanId.
     * @param deviceId The ID of device.
     * @param patientId The ID of patient.
     * @param careplanId The ID of careplan of patient.
     * @param deviceIdentifierSystem The system of device identifier.
     * @param patientIdentifierSystem The system of patient identifier.
     * @param carePlanIdentifierSystem The system of carePlan identifier.
     */
    constructor(deviceId: string, patientId: string, careplanId: string, deviceIdentifierSystem?: string, patientIdentifierSystem?: string, carePlanIdentifierSystem?: string);
    /**
     * Create and return a FHIR Device with current deviceId.
     * @returns FHIR Device with current deviceId.
     */
    private createDevice;
    /**
     * Set the PUT id for current device and returns it.
     * @returns The current device with id attribute set as current deviceId.
     */
    private setDevicePUTId;
    /**
     * Create and return a FHIR Patient with current patientId.
     * @returns FHIR Patient with current patientId.
     */
    private createPatient;
    /**
     * Return the patient.
     * @returns Current patient.
     */
    getPatient(): Patient;
    /**
     * Return the device
     * @returns Current device.
     */
    getDevice(): Device;
    /**
     * Return the patient ID.
     * @returns Current patientId.
     */
    getPatientId(): string;
    /**
     * Return the device ID.
     * @returns Current deviceId.
     */
    getDeviceId(): string;
    /**
     * Return the careplan ID.
     * @returns Current carePlanId.
     */
    getCarePlanId(): string;
    /**
     * Convert a standard event to a FHIR Observation.
     * @param event standard event to be converted to observation.
     * @returns FHIR Observation of standard event.
     */
    private standardEventToFHIR;
    /**
     * Package a list of FHIR Observations to a FHIR Bundle.
     * @param observations List of FHIR Observations to be packaged.
     * @returns FHIR Bundle with current patient, passed observation and current device.
     */
    private packageObservations;
    /**
     * Create a FHIR Bundle from Activity measurement.
     * @param startTime Start time of the measurement in unix timestamp (milliseconds) format.
     * @param endTime  End time of the measurement in unix timestamp (milliseconds) format.
     * @param activity Type of activity.
     * @param confidence Confidence of measurement.
     * @returns FHIR Bundle consisting of current patient, Activity as FHIR observation and current device.
     */
    convertActivityEvent(startTime: number, endTime: number, activity: ActivityType, confidence?: ActivityConfidence): Bundle;
    /**
     * Create a FHIR Bundle from GPS Location measurement.
     * @param time The time of measurement in unix timestamp (milliseconds) format.
     * @param latitude The latitude of GPS measurement.
     * @param longitude The longitude of GPS measurement.
     * @param altitude The altitude of GPS measurement.
     * @param horizontalAccuracy The horizontal accuracy of the measurement.
     * @returns FHIR Bundle consisting of current patient, GPS Location as FHIR observation and current device.
     */
    convertGPSLocationEvent(time: number, latitude: number, longitude: number, altitude?: number, horizontalAccuracy?: number): Bundle;
    /**
     * Create a FHIR Bundle from HeartRate measurement.
     * @param startTime Start time of the measurement in unix timestamp (milliseconds) format.
     * @param endTime  End time of the measurement in unix timestamp (milliseconds) format.
     * @param averageHeartRate Average heartRate over the measurement period.
     * @param minimumHeartRate Minimum heartRate over the measurement period.
     * @param maximumHeartRate Maximum heartRate over the measurement period.
     * @returns FHIR Bundle consisting of current patient, HeartRate as FHIR observation and current device.
     */
    convertHeartRateEvent(startTime: number, endTime: number, averageHeartRate: number, minimumHeartRate?: number, maximumHeartRate?: number): Bundle;
    /**
     * Create a FHIR Bundle from StepCount measurement.
     * @param startTime Start time of the measurement in unix timestamp (milliseconds) format.
     * @param endTime  End time of the measurement in unix timestamp (milliseconds) format.
     * @param stepCount The number of steps measured in the period of measurement.
     * @returns FHIR Bundle consisting of current patient, StepCount as FHIR observation and current device.
     */
    convertStepCountEvent(startTime: number, endTime: number, stepCount: number): Bundle;
    /**
     * Add events to the event list to be bundled into a single FHIR Bundle.
     * @param event The event to be added to the event list.
     * @returns The updated MeasurementToFHIRConverter object.
     */
    private addEvent;
    /**
     * Get the events currently added to the MeasurementToFHIRConverter Object.
     * @returns An array of added events.
     */
    getEvents(): MeasurementEvent[];
    /**
     * Clears the currently added events to the MeasurementToFHIRConverter Object.
     */
    clearAddedEvents(): void;
    /**
     * Pops the last added event.
     */
    popLastAddedEvent(): MeasurementEvent;
    /**
     * Add a measurement of type Activity to the event list of he MeasurementToFHIRConverter Object.
     * @param startTime Start time of the measurement in unix timestamp (milliseconds) format.
     * @param endTime  End time of the measurement in unix timestamp (milliseconds) format.
     * @param activity Type of activity.
     * @param confidence Confidence of measurement.
     * @returns The updated MeasurementToFHIRObject.
     */
    addActivityEvent(startTime: number, endTime: number, activity: ActivityType, confidence?: ActivityConfidence): MeasurementToFHIRConverter;
    /**
     * Add a measurement of type GPSLocation to the event list of he MeasurementToFHIRConverter Object.
     * @param time The time of measurement in unix timestamp (milliseconds) format.
     * @param latitude The latitude of GPS measurement.
     * @param longitude The longitude of GPS measurement.
     * @param altitude The altitude of GPS measurement.
     * @param horizontalAccuracy The horizontal accuracy of the measurement.
     * @returns The updated MeasurementToFHIRObject.
     */
    addGPSLocationEvent(time: number, latitude: number, longitude: number, altitude?: number, horizontalAccuracy?: number): MeasurementToFHIRConverter;
    /**
     * Add a measurement of type HeartRate to the event list of he MeasurementToFHIRConverter Object.
     * @param startTime Start time of the measurement in unix timestamp (milliseconds) format.
     * @param endTime  End time of the measurement in unix timestamp (milliseconds) format.
     * @param averageHeartRate Average heartRate over the measurement period.
     * @param minimumHeartRate Minimum heartRate over the measurement period.
     * @param maximumHeartRate Maximum heartRate over the measurement period.
     * @returns The updated MeasurementToFHIRObject.
     */
    addHeartRateEvent(startTime: number, endTime: number, averageHeartRate: number, minimumHeartRate?: number, maximumHeartRate?: number): MeasurementToFHIRConverter;
    /**
     * Add a measurement of type StepCount to the event list of he MeasurementToFHIRConverter Object.
     * @param startTime Start time of the measurement in unix timestamp (milliseconds) format.
     * @param endTime  End time of the measurement in unix timestamp (milliseconds) format.
     * @param stepCount The number of steps measured in the period of measurement.
     * @returns The updated MeasurementToFHIRObject.
     */
    addStepCountEvent(startTime: number, endTime: number, stepCount: number): MeasurementToFHIRConverter;
    /**
     * Bundles the added events to a FHIR Bundle and clears them from the MeasurementToFHIRObject.
     */
    bundleAddedEvents(): Bundle;
}
export { ActivityType, ActivityConfidence };
