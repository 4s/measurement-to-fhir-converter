/**
 * Enumeration for different types of measurement.
 */
export enum MeasurementEventType {
    /**
     * Measurement of type StepCount.
     */
    STEPCOUNT = "STEPCOUNT",
    /**
     * Measurement of type GPS Location.
     */
    GPSLOCATION = "GEOLOCATION",
    /**
     * Measurement of type Activity.
     */
    ACTIVITY = "ACTIVITY",
    /**
     * Measurement of type HeartRate.
     */
    HEARTRATE = "HEARTRATE"
}

/**
 * Generic class for measurement events
 */
export class MeasurementEvent {
    /**
     * Type of event
     * @private
     * @type {MeasurementEventType}
     * @ignore
     */
    private type: MeasurementEventType;

    /**
     * Time of measurement
     * @private
     * @type {number}
     * @ignore
     */
    private time: number;

    /**
     * Start time of period of measurement.
     * @private
     * @type {number}
     * @ignore
     */
    private startTime: number;

    /**
     * End time of period of measurement.
     * @private
     * @type {number}
     * @ignore
     */
    private endTime: number;

    /**
     * Constructor for MeasurementEvent
     * @param type The type of measurement.
     * @param time The time of the measurement in unix timestamp (milliseconds) format.
     * @param startTime The startTime of the measurement in unix timestamp (milliseconds) format.
     * @param endTime The endTime of the measurement in unix timestamp (milliseconds) format.
     */
    public constructor(type: MeasurementEventType, time?: number, startTime?: number, endTime?: number) {
        this.type = type;
        if (!(startTime == null) && !(endTime == null)) {
            if (startTime < 0) throw new Error('Start time must be zero or positive.');
            if (endTime < 0) throw new Error('End time must be zero or positive.');
            if (startTime > endTime) throw new Error('Start time must be less than or equal to end time.');
            this.startTime = startTime;
            this.endTime = endTime;
        }
        else if (!(time == null)) {
            if (time < 0) throw new Error('Time must be zero or positive!');
            this.time = time;
        }
    }

    /**
     * Get the type of the measurement.
     * @returns The type of the measurement.
     */
    public getType(): MeasurementEventType {
        return this.type;
    }

    /**
     * Get the time of measurement.
     * @returns The time of the measurement in unix timestamp (milliseconds) format.
     */
    public getTime(): number {
        return this.time;
    }

    /**
     * Get the startTime of the period of measurement in unix timestamp (milliseconds) format.
     * @returns The startTime of the period of measurement in unix timestamp (milliseconds) format.
     */
    public getStartTime(): number {
        return this.startTime;
    }

    /**
     * Get the endTime of the period of measurement in unix timestamp (milliseconds) format.
     * @returns The endTime of the period of measurement in unix timestamp (milliseconds) format.
     */
    public getEndTime(): number {
        return this.endTime;
    }
}


