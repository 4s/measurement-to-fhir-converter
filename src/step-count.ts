import { MeasurementEvent, MeasurementEventType } from "./measurement-event";

/**
 * Class representing measurement of type stepCount.
 */
export default class StepCount extends MeasurementEvent {

    /**
     * The number of steps.
     * @private
     * @type {number}
     * @ignore
     */
    private stepCount: number;

    /**
     * Create a StepCount MeasurementEvent
     * @param startTime Start time of the measurement in unix timestamp (milliseconds) format.
     * @param endTime  End time of the measurement in unix timestamp (milliseconds) format.
     * @param stepCount The number of steps measured in the period of measurement. 
     */
    public constructor(startTime: number, endTime: number, stepCount: number) {
        super(MeasurementEventType.STEPCOUNT, undefined, startTime, endTime);
        this.stepCount = stepCount;
    }

    /**
     * Get the stepCount.
     * @returns The stepCount.
     */
    public getStepCount(): number {
        return this.stepCount;
    }
}