import { MeasurementEvent } from "./measurement-event";
/**
 * Class representing measurement of type location.
 */
export default class GPSLocation extends MeasurementEvent {
    /**
     * Longitude component of GPS measurement.
     * @private
     * @type {number}
     * @ignore
     */
    private longitude;
    /**
     * Latitude component of GPS measurement.
     * @private
     * @type {number}
     * @ignore
     */
    private latitude;
    /**
     * Altitude component of GPS measurement.
     * @private
     * @type {number}
     * @ignore
     */
    private altitude;
    /**
     * Horizontal accuracy of the measurement.
     * @private
     * @type {number}
     * @ignore
     */
    private horizontalAccuracy;
    /**
     * Create a GPSLocation measurementEvent
     * @param time The time of measurement in unix timestamp (milliseconds) format.
     * @param latitude The latitude of GPS measurement.
     * @param longitude The longitude of GPS measurement.
     * @param altitude The altitude of GPS measurement.
     * @param horizontalAccuracy The horizontal accuracy of the measurement.
     */
    constructor(time: number, latitude: number, longitude: number, altitude?: number, horizontalAccuracy?: number);
    /**
     * Get the latitude of GPS measurement.
     * @returns The latitude of GPS measurement.
     */
    getLatitude(): number;
    /**
     * Get the longitude of GPS measurement.
     * @returns The longitude of GPS measurement.
     */
    getLongitude(): number;
    /**
     * Get the altitude of GPS measurement.
     * @returns The altitude of GPS measurement.
     */
    getAltitude(): number;
    /**
     * Get the accuracy of latitude of GPS measurement.
     * @returns The accuracy of latitude of GPS measurement.
     */
    getHorizontalAccuracy(): number;
}
