import { MeasurementEvent, MeasurementEventType } from "./measurement-event";

/**
 * Class representing measurement of type location.
 */
export default class GPSLocation extends MeasurementEvent {

    /**
     * Longitude component of GPS measurement.
     * @private
     * @type {number}
     * @ignore
     */
    private longitude: number;
    /**
     * Latitude component of GPS measurement.
     * @private
     * @type {number}
     * @ignore
     */
    private latitude: number;
    /**
     * Altitude component of GPS measurement.
     * @private
     * @type {number}
     * @ignore
     */
    private altitude: number;
    /**
     * Horizontal accuracy of the measurement.
     * @private
     * @type {number}
     * @ignore
     */
    private horizontalAccuracy: number;


    /**
     * Create a GPSLocation measurementEvent
     * @param time The time of measurement in unix timestamp (milliseconds) format.
     * @param latitude The latitude of GPS measurement.
     * @param longitude The longitude of GPS measurement.
     * @param altitude The altitude of GPS measurement.
     * @param horizontalAccuracy The horizontal accuracy of the measurement.
     */
    constructor(time: number, latitude: number, longitude: number, altitude?: number, horizontalAccuracy?: number) {
        super(MeasurementEventType.GPSLOCATION, time);
        this.latitude = latitude;
        this.longitude = longitude;
        this.altitude = altitude;
        this.horizontalAccuracy = horizontalAccuracy;
    }

    /**
     * Get the latitude of GPS measurement.
     * @returns The latitude of GPS measurement.
     */
    public getLatitude(): number {
        return this.latitude;
    }
    /**
     * Get the longitude of GPS measurement.
     * @returns The longitude of GPS measurement.
     */
    public getLongitude(): number {
        return this.longitude;
    }
    /**
     * Get the altitude of GPS measurement.
     * @returns The altitude of GPS measurement.
     */
    public getAltitude(): number {
        return this.altitude;
    }
    /**
     * Get the accuracy of latitude of GPS measurement.
     * @returns The accuracy of latitude of GPS measurement.
     */
    public getHorizontalAccuracy(): number {
        return this.horizontalAccuracy;
    }
}