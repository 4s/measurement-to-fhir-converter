/**
 * Enumeration for different types of measurement.
 */
export declare enum MeasurementEventType {
    /**
     * Measurement of type StepCount.
     */
    STEPCOUNT = "STEPCOUNT",
    /**
     * Measurement of type GPS Location.
     */
    GPSLOCATION = "GEOLOCATION",
    /**
     * Measurement of type Activity.
     */
    ACTIVITY = "ACTIVITY",
    /**
     * Measurement of type HeartRate.
     */
    HEARTRATE = "HEARTRATE"
}
/**
 * Generic class for measurement events
 */
export declare class MeasurementEvent {
    /**
     * Type of event
     * @private
     * @type {MeasurementEventType}
     * @ignore
     */
    private type;
    /**
     * Time of measurement
     * @private
     * @type {number}
     * @ignore
     */
    private time;
    /**
     * Start time of period of measurement.
     * @private
     * @type {number}
     * @ignore
     */
    private startTime;
    /**
     * End time of period of measurement.
     * @private
     * @type {number}
     * @ignore
     */
    private endTime;
    /**
     * Constructor for MeasurementEvent
     * @param type The type of measurement.
     * @param time The time of the measurement in unix timestamp (milliseconds) format.
     * @param startTime The startTime of the measurement in unix timestamp (milliseconds) format.
     * @param endTime The endTime of the measurement in unix timestamp (milliseconds) format.
     */
    constructor(type: MeasurementEventType, time?: number, startTime?: number, endTime?: number);
    /**
     * Get the type of the measurement.
     * @returns The type of the measurement.
     */
    getType(): MeasurementEventType;
    /**
     * Get the time of measurement.
     * @returns The time of the measurement in unix timestamp (milliseconds) format.
     */
    getTime(): number;
    /**
     * Get the startTime of the period of measurement in unix timestamp (milliseconds) format.
     * @returns The startTime of the period of measurement in unix timestamp (milliseconds) format.
     */
    getStartTime(): number;
    /**
     * Get the endTime of the period of measurement in unix timestamp (milliseconds) format.
     * @returns The endTime of the period of measurement in unix timestamp (milliseconds) format.
     */
    getEndTime(): number;
}
