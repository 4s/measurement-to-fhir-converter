import { MeasurementEvent } from "./measurement-event";
/**
 * Class for measurement of type heartRate.
 */
export default class HeartRate extends MeasurementEvent {
    /**
     * The average heartRate over the period of measurement.
     * @private
     * @type {number}
     * @ignore
     */
    private average;
    /**
     * The minimum heartRate over the period of measurement.
     * @private
     * @type {number}
     * @ignore
     */
    private minimum;
    /**
     * The maximum heartRate over the period of measurement.
     * @private
     * @type {number}
     * @ignore
     */
    private maximum;
    /**
     * Create a HeartRate measurementEvent
     * @param startTime Start time of the measurement in unix timestamp (milliseconds) format.
     * @param endTime  End time of the measurement in unix timestamp (milliseconds) format.
     * @param averageHeartRate Average heartRate over the measurement period.
     * @param minimumHeartRate Minimum heartRate over the measurement period.
     * @param maximumHeartRate Maximum heartRate over the measurement period.
     */
    constructor(startTime: number, endTime: number, averageHeartRate: number, minimumHeartRate?: number, maximumHeartRate?: number);
    /**
     * Get the average heartRate over the period of measurement.
     * @returns The average heartRate over the period of measurement.
     */
    getAverageHeartRate(): number;
    /**
     * Get the minimum heartRate over the period of measurement.
     * @returns The minimum heartRate over the period of measurement.
     */
    getMinimumHeartRate(): number;
    /**
     * Get the maximum heartRate over the period of measurement.
     * @returns The maximum heartRate over the period of measurement.
     */
    getMaximumHeartRate(): number;
}
