import { MeasurementEvent, MeasurementEventType } from "./measurement-event";

/**
 * Class for measurement of type heartRate.
 */
export default class HeartRate extends MeasurementEvent {

    /**
     * The average heartRate over the period of measurement.
     * @private
     * @type {number}
     * @ignore
     */
    private average: number;
    /**
     * The minimum heartRate over the period of measurement.
     * @private
     * @type {number}
     * @ignore
     */
    private minimum: number;
    /**
     * The maximum heartRate over the period of measurement.
     * @private
     * @type {number}
     * @ignore
     */
    private maximum: number;

    /**
     * Create a HeartRate measurementEvent
     * @param startTime Start time of the measurement in unix timestamp (milliseconds) format.
     * @param endTime  End time of the measurement in unix timestamp (milliseconds) format.
     * @param averageHeartRate Average heartRate over the measurement period.
     * @param minimumHeartRate Minimum heartRate over the measurement period.
     * @param maximumHeartRate Maximum heartRate over the measurement period.
     */
    constructor(startTime: number, endTime: number, averageHeartRate: number, minimumHeartRate?: number, maximumHeartRate?: number) {
        super(MeasurementEventType.HEARTRATE, undefined, startTime, endTime);
        this.average = averageHeartRate;
        this.minimum = minimumHeartRate;
        this.maximum = maximumHeartRate;
    }

    /**
     * Get the average heartRate over the period of measurement.
     * @returns The average heartRate over the period of measurement.
     */
    public getAverageHeartRate() {
        return this.average;
    }

    /**
     * Get the minimum heartRate over the period of measurement.
     * @returns The minimum heartRate over the period of measurement.
     */
    public getMinimumHeartRate() {
        return this.minimum;
    }

    /**
     * Get the maximum heartRate over the period of measurement.
     * @returns The maximum heartRate over the period of measurement.
     */
    public getMaximumHeartRate() {
        return this.maximum;
    }
}