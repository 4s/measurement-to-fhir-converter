# Measurement To FHIR Converter

This library functions as an easy way to convert ios or android device measurements to FHIR Bundles.

This README.md will explain the basics of installing, building, using and testing this library.
Beyond that, the repository also contains a more detailed TypeDoc documentation in the form of an HTML page in the docs folder located in this library or at:
https://bitbucket.org/4s/measurement-to-fhir-converter/src/master/docs/

## Installation

This library can be installed in two ways; using npm or manually.

### Using npm

You need npm installed on your machine (https://www.npmjs.com/get-npm)

In your project directory type the command

```bash
$ npm install bitbucket:4s/measurement-to-fhir-converter
```

### Manual installation

The sourcecode of this repository is written in typescript and will therefore have to be transpiled to Javascript
in order to be used in a project.

A transpiled version is available in the dist folder. Therefore, to use the already built version of this library,
just download dist/measurement-to-fhir-converter.js and include it in your HTML file:

```html
<script src="path/to/measurement-to-fhir-converter/dist/measurement-to-fhir-converter.js"></script>
```

A sourcemap is also included in the dist-folder for debuggability.

### Usage

Example usage of the library is shown as follows:

```typescript
import MeasurementToFHIRConverter, { ActivityType, ActivityConfidence } from 'src/measurement-to-fhir-converter'

//Create the measurementToFHIRConverter with appropriate deviceId, patientId and carePlanId.
//You can also optionally pass deviceIdentifierSystem, patientIdentifierSystem and carePlanIdentifierSystem.
const measurementToFHIRConverter = new MeasurementToFHIRConverter(deviceId, patientId, carePlanId);

//Construct FHIR bundles from the measurements.

//NOTE: The time, startTime and endTime parmeters must be in unix timestamp (milliseconds) format.

//convertStepCountEvent(startTime,endTime,stepCount)
const stepCountBundle = measurementToFHIRConverter.convertStepCountEvent(0,5,26);

//convertGPSLocationEvent(time,latitude,longitude,altitude(optional),accuracyLatitude(optional),accuracyLongitude(optional))
const gpsLocationBundle = measurementToFHIRConverter.convertGPSLocationEvent(4,23,47,23,2,2);

//convertActivityEvent(startTime,endTime,activityType,activityConfidence(optional))
const activityBundle = measurementToFHIRConverter.convertActivityEvent(0,10,ActivityType.CYCLING,ActivityConfidence.HIGH);

//If you are using Javascript, you can also use strings to define the activity type and confidence
const activityBundle = measurementToFHIRConverter.convertActivityEvent(0,10,"CYCLING","HIGH");

//convertHeartRateEvent(startTime,endTime,averageHeartRate,minimumHeartRate(optional),maximumHeartRate(optional))
const heartRateBundle = measurementToFHIRConverter.convertHeartRateEvent(0,10,45,42,48);

//Package Multiple Events to a Single Bundle
measurementToFHIRConverter.addStepCountEvent(0, 1, 1);
measurementToFHIRConverter.addGPSLocationEvent(0, 23, 47);
measurementToFHIRConverter.addActivityEvent(0, 1, ActivityType.CYCLING, ActivityConfidence.HIGH);
measurementToFHIRConverter.addHeartRateEvent(0, 1, 56);
const finalBundle = measurementToFHIRConverter.bundleAddedEvents();

//You can also chain the addition of multiple events
const finalBundle = measurementToFHIRConverter.addStepCountEvent(0, 1, 1)
    .addGPSLocationEvent(0, 23, 47)
    .addActivityEvent(0, 1, ActivityType.CYCLING, ActivityConfidence.HIGH)
    .addHeartRateEvent(0, 1, 56)
    .bundleAddedEvents();

//NOTE: bundleAddedEvents() clears all added events.

//If you want to clear all added events you can use the clearAddedEvents() function.
measurementToFHIRConverter.addStepCountEvent(0, 1, 1);
measurementToFHIRConverter.addGPSLocationEvent(0, 23, 47);
measurementToFHIRConverter.clearAddedEvents();
measurementToFHIRConverter.addActivityEvent(0, 1, ActivityType.CYCLING, ActivityConfidence.HIGH);
measurementToFHIRConverter.addHeartRateEvent(0, 1, 56);
//The bundle will only contain activity and heartRate.
const finalBundle = measurementToFHIRConverter.bundleAddedEvents();


//You can also pop just the last added event.
measurementToFHIRConverter.popLastAddedEvent();
```

## Making changes and building

If changes are made to the sourcecode, the bundled files have to be rebuilt. Webpack (https://github.com/webpack/webpack)
and typescript (https://www.typescriptlang.org/) are used for bundling and transpiling,
and in order to use these frameworks, npm is required (https://www.npmjs.com/get-npm).

Follow the steps below in your terminal/command prompt to build the project:

1\. Download or clone the project directory onto your machine and cd into it:

```bash
git clone git@bitbucket.org:4s/measurement-to-fhir-converter.git && cd measurement-to-fhir-converter
```

2\. Install the abovementioned dependencies:

```bash
npm install
```

(2\.a)

```
**Make changes to the project**
```

3\. Build the project

```bash
npm run build
```

The output of the build process will be located in the dist-directory

## Testing

Testing is performed using the Mocha framework (https://https://mochajs.org/) coupled with the Chai assertion library
(https://www.chaijs.com/). Webpack (https://github.com/webpack/webpack)
and babel (https://www.typescriptlang.org/) are used for bundling and transpiling respectively.

Therefore, to perform tests, you need to have npm installed on your machine (https://www.npmjs.com/get-npm)

To perform the tests follow the steps below in your terminal/command prompt:

1\. Download or clone the project directory onto your machine and cd into it:

```bash
git clone git@bitbucket.org:4s/measurement-to-fhir-converter.git && cd measurement-to-fhir-converter
```

2\. Install the abovementioned dependencies:

```bash
npm install
```

3\. Run the tests:

```bash
npm run test
```

The output from the test should appear directly in the terminal.

## Support & Ownership

Any questions regarding this library should be directed at [Harshit Mahapatra](mailto:harshit.mahapatra@alexandra.dk)
