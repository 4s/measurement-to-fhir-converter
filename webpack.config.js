const path = require('path');
module.exports = {
    entry: './src/measurement-to-fhir-converter.ts',
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/
            }
        ]
    },
    resolve: {
        extensions: [".tsx", ".ts", ".js"]
    },
    output: {
        library: 'measurement-to-fhir-converter',
        libraryTarget: 'umd',
        filename: 'measurement-to-fhir-converter.js',
        path: path.resolve(__dirname, 'dist')
    },
    devtool: "source-map"
};